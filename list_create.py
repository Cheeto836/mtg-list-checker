import requests
import json
import csv
import os.path
import argparse
import time

def getCardList(request_url, card_list):
    #get card list
    card_resp = json.loads(requests.get(request_url).text)
    for card in card_resp["data"]:
        card_list[card["collector_number"]] = card

    #check to see if the response was paginated
    if card_resp["has_more"]:
        time.sleep(.1) # wait for scryfall's specified time to not flood with requests
        getCardList(card_resp["next_page"], card_list)

    return card_resp["total_cards"]

def readList(file_path):
    if not os.path.isfile(file_path):
        print(file_path + " cannot be read")
        raise ValueError

    cards = set()
    for row in csv.DictReader(open(file_path)):
        cards.add(row["card number"])
    return cards


parser = argparse.ArgumentParser(description="Take in a list of card numbers and a set to generate a list of cards from that set")
parser.add_argument('--set', required=True, dest="set", help="Code for the set to compare the input list to")
parser.add_argument('--list', type=readList, required=True, dest="list", help="CSV list containing all of the card numbers to pull")
parser.add_argument('--out', required=True, dest="out", help="Location to write out the generated list")
args = parser.parse_args()

# ensure set is a real one
set_response = json.loads(requests.get("https://api.scryfall.com/sets/" + args.set).text)
if set_response["object"] == "error":
    print("Set: " + args.set + " is not a valid set.")
    exit()

# get all cards in the list
time.sleep(.1) # wait for scryfall's specified time to not flood with requests
cards = dict()
total_cards = getCardList("https://api.scryfall.com/cards/search?unique=prints&q=set:" + args.set, cards)

if total_cards != len(cards):
    print("something went wrong")
    exit()

with open(args.out + "/" + args.set + "_inventory.csv", 'w') as inventory_file:
    with open(args.out + "/" + args.set + "_missing.csv", 'w') as missing_file:
        inventory_file.write("card name, card number, rarity, type, scryfall link\n")
        missing_file.write("card name, card number, rarity, type, scryfall link\n")
        cur_num = 1
        while cur_num <= total_cards:
            if str(cur_num) not in cards:
                print("card number " + str(cur_num) + " is not in set " + args.set + "...skipping")
                continue

            cur_card = cards[str(cur_num)]
            if str(cur_num) in args.list:
                inventory_file.write(cur_card["name"] + ", " + cur_card["collector_number"] + ", " + cur_card["rarity"] + ", " + cur_card["type_line"] + ", " + cur_card["scryfall_uri"] + "\n")
            else:
                missing_file.write(cur_card["name"] + ", " + cur_card["collector_number"] + ", " + cur_card["rarity"] + ", " + cur_card["type_line"] + ", " + cur_card["scryfall_uri"] + "\n")

            cur_num += 1
